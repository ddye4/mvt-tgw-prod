variable "name_1" {
    type = string
    description = "Name of Virginia TGW"
    default = "tgw-prod-us-east-1"
}
variable "description_1" {
    type = string
    description = "Virginia Prod TGW"
    default = "Virginia Prod Transit Gateway"
}
variable "ram_name_1" {
    type = string
    description = "Name of Virginia TGW RAM Share"
    default = "tgw-ram-share-us-east-1"
}
variable "name_2" {
    type = string
    description = "Name of Oregon TGW"
    default = "tgw-prod-us-west-2"
}
variable "description_2" {
    type = string
    description = "Oregon Prod TGW"
    default = "Oregon Prod Transit Gateway"
}
variable "ram_name_2" {
    type = string
    description = "Name of Oregon TGW RAM Share"
    default = "tgw-ram-share-us-west-2"
}
variable "asn_1" {
    type = string
    description = "Provide Unique ASN For Virginia TGW"
}
variable "asn_2" {
    type = string
    description = "Provide Unique ASN For Oregon TGW"
}
variable "ram_principals" {
    type = string
    description = "Members Organizational Unit ARN"
    default = "arn:aws:organizations::477066157268:ou/o-684ps7q3wm/ou-gx9m-asce4sx8"
}
variable "peer-region" {
    type = string
    description = "TGW Peering Attachment to Oregon"
    default = "us-west-2"
}
variable "requestor-region" {
    type = string
    description = "TGW Attachment Requestor"
    default = "us-east-1"
}