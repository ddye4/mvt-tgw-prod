module "us-east-1" {
 	source = "terraform-aws-modules/transit-gateway/aws"
  version = "~> 2.5"

	name = var.name_1
	description = var.description_1
	amazon_side_asn = var.asn_1
  enable_auto_accept_shared_attachments = true

	ram_name = var.ram_name_1
	ram_principals = [var.ram_principals]

  providers = {
    aws = aws.east
  }
}

module "us-west-2" {
	source = "terraform-aws-modules/transit-gateway/aws"
  version = "~> 2.5"

	name = var.name_2
	description = var.description_2
	amazon_side_asn = var.asn_2
  enable_auto_accept_shared_attachments = true

	ram_name = var.ram_name_2
	ram_principals = [var.ram_principals]

  providers = {
    aws = aws.west
  }
}

resource "aws_ec2_transit_gateway_peering_attachment" "tgw-peering" {
  provider = aws.east
  peer_region = var.peer-region
  peer_transit_gateway_id = module.us-west-2.ec2_transit_gateway_id
  transit_gateway_id = module.us-east-1.ec2_transit_gateway_id
  tags = {
    Name = "Virginia TGW Peering Requestor"
    Side = "Creator"
  }
}

resource "aws_ec2_transit_gateway_peering_attachment_accepter" "tgw-peering" {
  provider = aws.west
  transit_gateway_attachment_id = aws_ec2_transit_gateway_peering_attachment.tgw-peering.id
  tags = {
    Name = "Oregon TGW Peering Acceptor"
    Side = "Acceptor"
  }
}