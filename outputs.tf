output "tgw-id-us-east-1" {
    value = module.us-east-1.ec2_transit_gateway_id
    description = "Virgina TGW ID"
}
output "tgw-id-us-west-2" {
    value = module.us-west-2.ec2_transit_gateway_id
    description = "Oregon TGW ID"
}
output "tgw-peering-attachment-id" {
    value = aws_ec2_transit_gateway_peering_attachment.tgw-peering.id
    description = "TGW Peering Attachment ID"
}